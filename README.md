# README #
Test for changes
### What is this repository for? ###

* This is a python program for generating a proposition towards airlines for the number of check-in desks needed.
* Version 3
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* You will need an imput file of a very certain configuration. This file should contain the flight info for a certain day. The number of flights, the number of passengers, the destination, the airline. Let's no lie about this, most of the work is done by the person preparing this file. 
* Configuration: Once the input file is prepared you can just run the simulation
* Dependencies: Python - tkinter, xlrd, xlwt


### Contribution guidelines ###

* Writing tests: euhmm...
* Code review: Harm Bossers

### Who do I talk to? ###

* Repo owner or admin: For any questions it is best to contact Willem Vande Gaer (willem.vandegaer@ortec.com)
* Other community or team contact: When not available you can also contact Bert Coonen with questions regarding running the application or business value.