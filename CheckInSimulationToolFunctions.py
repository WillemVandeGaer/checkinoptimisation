# -*- coding: utf-8 -*-
"""
@author: WillemG
"""

#to create the browse window
import tkinter as TK 
from tkinter import filedialog
import xlrd
import datetime as dt
import math

#This file contains the functions from the CheckInSimulationTool



class Flight ():
    
    #flight has a number, date, time, number of pax, 
    def __init__(self, number, date, time, pax, flightType):
        self.number = number
        self.date = date
        self.time = time
        self.pax = pax
        self.flightType = flightType
        
class Event ():
    
    #An event has a name, time
    def __init__(self, name, time, flight, extraInfo = ""):
        self.name = name
        self.time = time
        self.flight = flight
        self.extraInfo = extraInfo
        
#Open the input file
def OpenInputFile():
    #Import the data
    #Open the excel workbook
    root = TK.Tk()
    # TODO this should be changed to \ or netwerk drive or parameter
    root.filename =  filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("excel files","*.xlsx"),("all files","*.*")))
    root.mainloop

    print("open excel file ",root.filename)
    workbook = xlrd.open_workbook(root.filename)
    root.destroy()
    return workbook


#Initialise a dictionary with all the keys defined in the provided worksheet
def CreateDictionaryKeys(workSheet, dictionary, cell):
    
    #Create a dictionary for every type of check-in an empty list
    for index in range(1, workSheet.nrows):
        #0 is always the key to be used
        key = workSheet.cell(index,0).value
        #for the eventlist we need to create the keys, but not add anything
        if cell is None:
            dictionary[key] = []
        else:
            #Add the correct value to the correct key
            dictionary[key] = workSheet.cell(index,cell).value
    return dictionary

        
#Read the arrivalpatterns from the sheet in het workbook
def populateArrivalPatterns(ArrivalPatternSheet, arrivalPatternDict):
  
  #arrival pattern column nrs
  keyColNr = 0  #column A
  startColNr = 1  #column B
  endColNr = 24   #column Y
  nrCols = endColNr - startColNr + 1
  
  #Fill the arrivalPatterns from std-240 - std-230 - std-220 ...
  for indexRow in range(1, ArrivalPatternSheet.nrows):
      key = ArrivalPatternSheet.cell(indexRow,keyColNr).value
      arrivalPatternDict[key] = [0] * nrCols
      for indexCol in range(startColNr, endColNr+1):
          arrivalPatternDict[key][indexCol-1] = ArrivalPatternSheet.cell(indexRow,indexCol).value
  return arrivalPatternDict

# Populate the dictionary of locations
def populateLocationsDictionary(LocationsSheet, locationsDict):
    for index in range(1, LocationsSheet.nrows):
        locationsDict[LocationsSheet.cell(index,0).value] = [LocationsSheet.cell(index,7), LocationsSheet.cell(index,2)]
    return locationsDict


def populateFlightsSN(flightList, SNPaxSheet, locationsDict, onlineCheckInDict, workbook):
    #Fill in all the data for the SN flights
    for index in range(1, SNPaxSheet.nrows):
        print(index)
        flightType = getGeoLocationSN(locationsDict, SNPaxSheet.cell(index, 20).value)
        flightNumber = SNPaxSheet.cell(index,1).value
        date = SNPaxSheet.cell(index,0).value
        time = dt.datetime(*xlrd.xldate_as_tuple(SNPaxSheet.cell(index,17).value, workbook.datemode))
        numberOfPax = int(onlineCheckInDict[flightType]*int(SNPaxSheet.cell(index,9).value))
        flightList.append(Flight(flightNumber, date, time, numberOfPax, flightType))
    
    return flightList

#get the geographical location based of the airport
#this is used to get the SN flights in correct group
def getGeoLocationSN(locationsDict, destAirport):
    
    #Get the geographical location based on the geo code from the input file
    codes = {
    "NAM": "SNAmerica",
    "CAM": "SNAmerica",
    "SAM": "SNAmerica",
    "AFR": "SNAfrica"}
    #If not America or Africa it is Europe
    print(destAirport)
    flightType = codes.get(locationsDict[destAirport][0].value, "SNEurope")

    #exceptions
    #India is checked in with America
    if (locationsDict[destAirport][1].value == "IN"): flightType = "SNAmerica"
    #Maroc is with europe
    if (locationsDict[destAirport][1].value == "MA"): flightType = "SNEurope"
    
    return flightType

#Populate the flightlist with non-SN flights with correct data
def populateFlightsRest(flightList, paxSheet, locationsDict, onlineCheckInDict, workbook):
    
    for index in range(1, paxSheet.nrows):
        flightType = paxSheet.cell(index, 0).value
        flightNumber = paxSheet.cell(index,2).value
        date = paxSheet.cell(index,1).value
        time = dt.datetime(*xlrd.xldate_as_tuple(paxSheet.cell(index,4).value, workbook.datemode))
        numberOfPax = int(onlineCheckInDict[flightType]*int(paxSheet.cell(index,3).value))
        flightList.append(Flight(flightNumber, date, time, numberOfPax, flightType))
        
    return flightList

#Create all passengers with their arrival Times
def createArrivalEvents(flightList, arrivalPatternDict, eventDict):
    passengerCounter = 1
    for flight in flightList:
        
        #populate the list of all the arrivalHallEvents to start the program
        #iterate the arrival pattern
        for i, interval in enumerate(arrivalPatternDict[flight.flightType]):
            #calculate how many people arrive at this interval
            
            peopleThisInterval = math.ceil(interval*flight.pax)
            arrivalTime = flight.time - dt.timedelta(minutes = (240 - (10*i)))
            #create a new person with the correct arrivalTime
            for j in range(0, peopleThisInterval):
                #Factor to evenly spread the passengers over the interval
                spreadingFactor = j // (peopleThisInterval/10)
                eventDict[flight.flightType].append(Event("ArrivalQueue", arrivalTime + dt.timedelta(minutes = spreadingFactor), flight, passengerCounter))
                passengerCounter += 1
    return eventDict         

#output the arriving passengers
def printAllArrivingPax(eventDict, outputExcel):
    #The output files
    index = 1
    
    rawData = outputExcel.add_sheet("ArrivingPeopleRawData")
    rawData.write(0,0, "Key")
    rawData.write(0,1, "Date")
    rawData.write(0,2, "Time")
    rawData.write(0,3, "FlightNr")
    rawData.write(0,4, "PaxNr")
    for key in eventDict:

        for event in eventDict[key]:
            rawData.write(index, 0, key)
            rawData.write(index, 1, event.time.strftime("%Y-%m-%d"))
            rawData.write(index, 2, event.time.strftime("%H:%M:%S"))
            rawData.write(index, 3, event.flight.number)
            rawData.write(index, 4, event.extraInfo)
            index += 1
    return outputExcel
    
def printAllNumberOfDesks(outputDeskList, outputExcel):
    
    rawData = outputExcel.add_sheet("NumberOfDesks")
    rawData.write(0,0, "Key")
    rawData.write(0,1, "Date")
    rawData.write(0,2, "Time")
    rawData.write(0,3, "NumberOfDesks")
    rawData.write(0,4, "QueueLength")
    for index, record in enumerate(outputDeskList):
        rawData.write(index+1, 0, record[0])
        rawData.write(index+1, 1, record[1].strftime("%Y-%m-%d"))
        rawData.write(index+1, 2, record[1].strftime("%H:%M:%S"))
        rawData.write(index+1, 3, record[2])
        rawData.write(index+1, 4, record[3])
    return outputExcel


def printAllPerHalfHour(eventDict, outputDeskList, outputExcel):
    checkInTypesDict = {}
    halfHourDict = {}
    #create a dictionary of every half hour
    for i in range(24):
        halfHourDict[dt.time(hour = i, minute = 0)] = [None]
        halfHourDict[dt.time(hour = i, minute = 30)] = [None]
        
    #create a dictionary for every type of checking with a half hour dictionary
    for key in eventDict:
        checkInTypesDict[key] = halfHourDict.copy()
        for half in checkInTypesDict[key]:
            #create empty list of size 5
            checkInTypesDict[key][half] = [0] * 5
        
    #Count the number of events per key per half hour
    for key in eventDict:
        for event in eventDict[key]:
            #Get time per half hour (integer division times 30)
            time = dt.time(hour = event.time.hour,minute = ((event.time.minute)//30)*30)
            checkInTypesDict[key][time][0] += 1
    
    for record in outputDeskList:
        #per half hour (integer division times 30)
        time = dt.time(hour = record[1].hour,minute = ((record[1].minute)//30)*30)
        #Find the max number of desks for that half hour
        if record[2] > checkInTypesDict[record[0]][time][1]:    
            checkInTypesDict[record[0]][time][1] = record[2]
        #Find the max number of pax  for that half hour
        if record[3] > checkInTypesDict[record[0]][time][2]:    
            checkInTypesDict[record[0]][time][2] = record[3]    
        #Find the max queue time for that half hour
        if record[3] > checkInTypesDict[record[0]][time][3]:    
            checkInTypesDict[record[0]][time][3] = record[4]  
    
    index = 1
    rawData = outputExcel.add_sheet("HalfHourTable")
    rawData.write(0,0, "Key")
    rawData.write(0,1, "HalfHour")
    rawData.write(0,2, "Date")
    rawData.write(0,3, "MaxNumberOfDesks")
    rawData.write(0,4, "MaxPaxInQueue")
    rawData.write(0,5, "MaxQueueTime")
    rawData.write(0,6, "PaxArriving")
    for key in checkInTypesDict:
        for half in checkInTypesDict[key]:
            rawData.write(index, 0, key)
            rawData.write(index, 1, half.strftime("%H:%M:%S"))
            rawData.write(index, 2, outputDeskList[0][1].strftime("%Y-%m-%d"))
            rawData.write(index, 3, checkInTypesDict[key][half][1])
            rawData.write(index, 4, checkInTypesDict[key][half][2])
            rawData.write(index, 5, checkInTypesDict[key][half][3])
            rawData.write(index, 6, checkInTypesDict[key][half][0])
            index += 1
    
    return outputExcel
    



        

   