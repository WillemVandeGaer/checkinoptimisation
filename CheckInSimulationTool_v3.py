# -*- coding: utf-8 -*-
import datetime as dt

from collections import Counter
import CheckInSimulationToolFunctions as cf
import xlwt


#TODO Ortec Github nakijken - center of exelence contactpersoon
#TODO EXE bestand van maken
#TODO Geen magic number - constanten
#TODO LX afzonderlijk rij 3
#TODO input check - rapportering indien niet - getter & setter
#TODO get all exceptions for check-in
#TODO XLSX opslaan

#*******************************************
#This entire part of code is initialisation
#*******************************************


#create a list of events per type of check-in
#Holds all queueEvents per key
queueLengthDict = {}
#Holds all arrivalPatterns per key
checkInTimeDict = {}
#Holds the number of people doing online checking per key
onlineCheckInDict = {}
#Holds the arrivalpatterns per key
arrivalPatternDict = {}
#Dictionary containing all destinations (used to differentiate SN check-in)
locationsDict = {}
#Dictionary to keep track of all passengers that have checked-in to produce output
finishedDict = {}
#Dictionary holing all events
eventDict = {}

print("*******************")
print("**** Read data ****")
print("*******************")

#Select the input file
workbook = cf.OpenInputFile()

#Fill all the dictionaries
#Get the correct sheet from the input excel
InfoSheet = workbook.sheet_by_name("FlightDesks")
queueLengthDict = cf.CreateDictionaryKeys (InfoSheet, queueLengthDict, 1) # 1 = QueueLength
checkInTimeDict = cf.CreateDictionaryKeys (InfoSheet, checkInTimeDict, 2) # 2 = checkin-time
onlineCheckInDict = cf.CreateDictionaryKeys (InfoSheet, onlineCheckInDict, 3) # 3 = % that checks in online
eventDict = cf.CreateDictionaryKeys (InfoSheet, eventDict, None) #should be empty
finishedDict = cf.CreateDictionaryKeys (InfoSheet, finishedDict, None)

#Fill the arrivalPattern dictionary
#Get the correct sheet from the input excel
arrivalPatternSheet = workbook.sheet_by_name("ArrivalPattern")
arrivalPatternDict = cf.populateArrivalPatterns(arrivalPatternSheet, arrivalPatternDict)

#create a dictionary for all the destinations to select the correct arrival pattern
#name is the airport [GeoLocationCode , County (for exeptions)]
LocationsWorksheet = workbook.sheet_by_name("Locations")
locationsDict = cf.populateLocationsDictionary(LocationsWorksheet, locationsDict)

#The list containing all the flights, with all data
flightList = []

#Add all SN flights to flightList
#For SN the flights should be divided into Eur / Afr / Am
SNResPaxSheet = workbook.sheet_by_name("ResPaxSN")
flightList = cf.populateFlightsSN(flightList, SNResPaxSheet, locationsDict, onlineCheckInDict, workbook)


#Add all other flights to fligthList
ResPaxRestSheet = workbook.sheet_by_name("ResPaxRest")
flightList = cf.populateFlightsRest(flightList, ResPaxRestSheet, locationsDict, onlineCheckInDict, workbook)

#initialise the events (arriving passengers)
eventDict = cf.createArrivalEvents(flightList, arrivalPatternDict, eventDict)

#****************************
#* End of Code initialisation
#****************************

print("*******************")
print("* Start main loop *")
print("*******************")

#****************************
#* Start the calculations
#****************************

#list used for output of number of desks
outputDeskList = []
#iterate all different check-in types
for key in eventDict:
    
    #if there are no people for this type of check-in skip it
    if (len(eventDict[key]) == 0):
        continue

    print("start of ",key)

    #Add the first line for the output
    outputTuple = (key, eventDict[key][0].time - dt.timedelta(minutes = 1), 0, 0)
    outputDeskList.append(outputTuple)

    #The maximum number of desks
    maxDesks = queueLengthDict[key]
    
    #The number of desks currently open
    currentDesks = 1
    
    #The number of desks in use atm
    desksUsed = 0
    
    #The checkin time
    checkInTime = dt.timedelta(seconds = checkInTimeDict[key])
    
    #create a list with all the events
    eventList = eventDict[key].copy()
    #List of people in queue
    queueList = []
    #for event in eventList: print(event.time)
    
    #!This is the loop that will keep on running untill everybody has checked in
    while len(eventList) > 0:
        eventList.sort(key=lambda x: x.time, reverse=False)
        event = eventList[0]
        
        
        #Calculate the queue time
        queueTime = round((len(queueList) * (checkInTimeDict[key]/60)) / currentDesks)
            
        #Check to open a new desk
        if len(queueList) > 0:
            
            #print(key,"queueTime = ",queueTime, " len(queueList)= ", len(queueList), "checkinTime = ",(checkInTimeDict[key]/60))
            #If the waiting time is bigger than 15 minutes open a new desk
            if queueTime > 15 and currentDesks < maxDesks:
                currentDesks = currentDesks + 1                      
                print(key, "currentDesks= ", currentDesks, event.time, maxDesks)
            #If the waiting time is smaller than 5 minutes close a desk
            if queueTime < 5 and currentDesks > 1:
                currentDesks = currentDesks - 1
                print(key, "currentDesks= ", currentDesks, event.time, maxDesks)      
        
        #If the event is ArrivalQueue
        if event.name == "ArrivalQueue":
            #Check for free desk
            if desksUsed < currentDesks:
                #If there is no queue
                if len(queueList) == 0:
                    print("Person ",event.extraInfo," can go directly to desk at ",event.time)
                    #create a new leaveDeskEvent
                    newEvent = cf.Event("LeaveDesk", event.time + checkInTime,event.flight, event.extraInfo)
                    eventList.append(newEvent)
                    #remove the queueEvent
                    eventList.remove(event)
                    desksUsed += 1
                    #Prepare outputFile
                    outputTuple = (key, event.time, currentDesks, len(queueList), queueTime)
                    outputDeskList.append(outputTuple)
                    continue
                else:
                    #IF there is a queue
                    #add the first person of the queue to the desk
                    print("Person ", queueList[0].extraInfo, " can check in now, and will be ready at ",eventList[0].time + checkInTime)
                    #create a new leaveDeskEvent
                    newEvent = cf.Event("LeaveDesk", eventList[0].time + checkInTime, queueList[0].flight, queueList[0].extraInfo)
                    eventList.append(newEvent)
                    #put the new arrivalEvent at the end of the queue
                    queueList.append(event)
                    eventList.remove(event)
                    queueList.pop(0)
                    desksUsed += 1
                    #Prepare outputFile
                    outputTuple = (key, event.time, currentDesks, len(queueList), queueTime)
                    outputDeskList.append(outputTuple)
                    
            #If there is no free desk
            else:
                #Remove the event from the eventList and add to the queueList
                print("Person ", event.extraInfo," waits in queue now", event.time)
                queueList.append(event)
                eventList.remove(event)
                continue
            
        #If the event is LeaveDesk
        if event.name == "LeaveDesk":
            #IF there is queue add the first person to this desk
            if len(queueList) > 0:
                #remove the leaveDeskEvent
                eventList.remove(event)
                #Clear the desk and add the first person in queue to the desk
                #create a new LeaveDeskEvent from the first person in queue
                #The next leavedesk is when this leavedesk ends + checkIn Time
                newEvent = cf.Event("LeaveDesk", event.time + checkInTime ,queueList[0].flight, queueList[0].extraInfo)
                eventList.append(newEvent)
                #remove the queue event that has become a leaveDesk event
                queueList.pop(0)
                
                #Prepare the output file
                outputTuple = (key, event.time, currentDesks, len(queueList), queueTime)
                outputDeskList.append(outputTuple)
                print("Person ",event.extraInfo," is done checking in at ",event.time)
                print("Person ",newEvent.extraInfo, " starts checking in now and will be ready at", newEvent.time)
                
                continue
               
            #If there is no queue
            else:
                #1 desk less in use
                desksUsed = desksUsed - 1
                #Remove the event
                eventList.remove(event)
                
                
                #prepare the output file
                outputTuple = (key, event.time, currentDesks, len(queueList), queueTime)
                outputDeskList.append(outputTuple)
                print("Person ",event.extraInfo," is (last one) done checking in at ",event.time)
                
                
                continue
               
    print("End of ",key )
    #Add the last line for the output
    outputTuple = (key, outputDeskList[-1][1] + dt.timedelta(minutes = 1), 0, 0, 0)
    outputDeskList.append(outputTuple)


print("*******************")
print("** End main loop **")
print("*******************")

#Create the excel file for export
outputExcel = xlwt.Workbook()  

outputExcel = cf.printAllArrivingPax(eventDict, outputExcel)
outputExcel = cf.printAllNumberOfDesks(outputDeskList, outputExcel)
outputExcel = cf.printAllPerHalfHour(eventDict, outputDeskList, outputExcel)

outputExcel.save("CheckInCalculations.xls")

print("*******************")
print("*** End program ***")
print("*******************")


        


#print(len(checkedList))